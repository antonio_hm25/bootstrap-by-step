const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const sass = require('gulp-sass');

// Compila sass into css and auto-inject into browsers
gulp.task('sass', function () {
  let src = [
    'src/scss/*.scss'
  ];

  return gulp.src(src)
             .pipe(sass())
             .pipe(gulp.dest('src/css'))
             .pipe(browserSync.stream());
});

// Move to javascript files into our /src/js folder
gulp.task('js', function () {
  let src = [
    'node_modules/jquery/dist/jquery.js',
    'node_modules/popper.js/dist/umd/popper.js',
    'node_modules/bootstrap/dist/js/bootstrap.js'
  ];

  return gulp.src(src)
             .pipe(gulp.dest('src/js'))
             .pipe(browserSync.stream());
});

// Static Server + watching scss/html files
gulp.task('serve', ['sass'], function () {
  browserSync.init({
    server: './src'
  });

  let watchSass = [
    'src/scss/*.scss'
  ];

  gulp.watch(watchSass, ['sass']);
  gulp.watch('src/*.html').on('change', browserSync.reload);
});

gulp.task('default', ['js', 'serve']);
